<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>com.ftd</groupId>
    <artifactId>partner-authentication-service</artifactId>
    <version>0.0.1-SNAPSHOT</version>
    <packaging>jar</packaging>

    <name>partner-authentication-service</name>
    <description>partner authentication service</description>

    <parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>1.5.9.RELEASE</version>
        <relativePath/>
        <!-- lookup parent from repository -->
    </parent>


    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
        <java.version>1.8</java.version>
        <spring-cloud.version>Dalston.SR3</spring-cloud.version>
        <!--<swagger.version>2.7.0</swagger.version>-->
        <maven.jar.plugin.version>3.0.2</maven.jar.plugin.version>
        <jacoco.version>0.7.5.201505241946</jacoco.version>
        <restAssured.version>3.0.7</restAssured.version>
        <ftd-commons.version>0.0.1-SNAPSHOT</ftd-commons.version>
        <centralRepo>http://artifactory.ftdi.com/artifactory/libs-release-local</centralRepo>
        <snapshotRepo>http://artifactory.ftdi.com/artifactory/libs-snapshot-local</snapshotRepo>
    </properties>

    <repositories>
        <!-- <repository>
            <id>nexus</id>
             <url>http://35.185.85.217/nexus/content/repositories/releases</url>
         </repository> -->

        <repository>
            <id>private-repo</id>
            <url>${centralRepo}</url>
        </repository>
        <repository>
            <id>private-repo-snapshots</id>
            <url>${snapshotRepo}</url>
        </repository>
    </repositories>

    <distributionManagement>
        <repository>
            <id>private-repo</id>
            <name>ftddev-releases</name>
            <url>${centralRepo}</url>
        </repository>
        <snapshotRepository>
            <id>private-repo-snapshots</id>
            <name>ftddev-snapshots</name>
            <url>${snapshotRepo}</url>
        </snapshotRepository>
    </distributionManagement>

    <dependencies>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-actuator</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-validation</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-configuration-processor</artifactId>
            <optional>true</optional>
        </dependency>
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-hystrix</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-hystrix-dashboard</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-sleuth</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-zipkin</artifactId>
        </dependency>
        <dependency>
            <groupId>org.apache.httpcomponents</groupId>
            <artifactId>httpclient</artifactId>
        </dependency>
        <dependency>
            <groupId>org.apache.httpcomponents</groupId>
            <artifactId>fluent-hc</artifactId>
            <version>4.5.4</version>
        </dependency>
        <dependency>
            <groupId>com.fasterxml.jackson.core</groupId>
            <artifactId>jackson-core</artifactId>
        </dependency>
        <dependency>
            <groupId>com.fasterxml.jackson.dataformat</groupId>
            <artifactId>jackson-dataformat-xml</artifactId>
        </dependency>
        <dependency>
            <groupId>com.fasterxml.jackson.core</groupId>
            <artifactId>jackson-databind</artifactId>
        </dependency>
        <dependency>
            <groupId>com.fasterxml.jackson.core</groupId>
            <artifactId>jackson-annotations</artifactId>
        </dependency>
        <dependency>
            <groupId>commons-beanutils</groupId>
            <artifactId>commons-beanutils</artifactId>
            <version>1.9.3</version>
        </dependency>
        <!-- https://mvnrepository.com/artifact/commons-validator/commons-validator -->
        <dependency>
            <groupId>commons-validator</groupId>
            <artifactId>commons-validator</artifactId>
            <version>1.6</version>
        </dependency>
        <dependency>
            <groupId>org.springframework.integration</groupId>
            <artifactId>spring-integration-test</artifactId>
        </dependency>
        <dependency>
            <groupId>io.rest-assured</groupId>
            <artifactId>rest-assured</artifactId>
            <version>${restAssured.version}</version>
            <!-- CARL -->
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>io.rest-assured</groupId>
            <artifactId>json-path</artifactId>
            <version>${restAssured.version}</version>

        </dependency>
        <dependency>
            <groupId>io.rest-assured</groupId>
            <artifactId>xml-path</artifactId>
            <version>${restAssured.version}</version>
        </dependency>
        <dependency>
            <groupId>io.rest-assured</groupId>
            <artifactId>json-schema-validator</artifactId>
            <version>${restAssured.version}</version>
        </dependency>
        <dependency>
            <groupId>io.rest-assured</groupId>
            <artifactId>spring-mock-mvc</artifactId>
            <version>${restAssured.version}</version>
        </dependency>
        <dependency>
            <groupId>com.ftd</groupId>
            <artifactId>commons-metric</artifactId>
            <version>${ftd-commons.version}</version>
        </dependency>
        <dependency>
            <groupId>com.ftd</groupId>
            <artifactId>commons-git</artifactId>
            <version>${ftd-commons.version}</version>
        </dependency>
        <dependency>
            <groupId>com.ftd</groupId>
            <artifactId>commons-swagger</artifactId>
            <version>${ftd-commons.version}</version>
        </dependency>
        <dependency>
            <groupId>com.ftd</groupId>
            <artifactId>commons-logging</artifactId>
            <version>${ftd-commons.version}</version>
        </dependency>
        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <version>1.18.4</version>
        </dependency>
        <!-- Mongo -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-data-mongodb</artifactId>
            <exclusions>
                <exclusion>
                    <groupId>org.mongodb</groupId>
                    <artifactId>mongodb-driver</artifactId>
                </exclusion>
            </exclusions>
        </dependency>
        <dependency>
            <groupId>org.mongodb</groupId>
            <artifactId>mongodb-driver</artifactId>
            <version>3.9.0</version>
        </dependency>
        <dependency>
            <groupId>com.ftd</groupId>
            <artifactId>commons-https</artifactId>
            <version>${ftd-commons.version}</version>
        </dependency>

        <!-- https://mvnrepository.com/artifact/org.apache.commons/commons-lang3 -->
        <dependency>
            <groupId>org.apache.commons</groupId>
            <artifactId>commons-lang3</artifactId>
            <version>3.7</version>
        </dependency>


        <dependency>
            <groupId>com.ftd</groupId>
            <artifactId>commons-misc</artifactId>
            <version>${ftd-commons.version}</version>
        </dependency>
        <dependency>
            <groupId>com.google.code.gson</groupId>
            <artifactId>gson</artifactId>
            <version>2.8.2</version>
        </dependency>
    </dependencies>

    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>org.springframework.cloud</groupId>
                <artifactId>spring-cloud-dependencies</artifactId>
                <version>${spring-cloud.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <build>
        <plugins>
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
            </plugin>
            <plugin>
                <groupId>pl.project13.maven</groupId>
                <artifactId>git-commit-id-plugin</artifactId>
                <version>2.2.1</version>
                <configuration>
                    <failOnNoGitDirectory>false</failOnNoGitDirectory>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-checkstyle-plugin</artifactId>
                <version>2.17</version>
                <configuration>
                    <sourceDirectory>${project.build.sourceDirectory}</sourceDirectory>
                </configuration>
                <executions>
                    <execution>
                        <id>check</id>
                        <phase>validate</phase>
                        <goals>
                            <goal>check</goal>
                            <!--<goal>checkstyle</goal> -->
                        </goals>
                        <configuration>
                            <configLocation>${basedir}/checkstyle.xml</configLocation>
                            <encoding>UTF-8</encoding>
                            <consoleOutput>true</consoleOutput>
                            <failsOnError>true</failsOnError>
                            <failOnViolation>true</failOnViolation>
                            <excludes>**/generated/**/*</excludes>
                            <includeTestSourceDirectory>false</includeTestSourceDirectory>
                            <!-- CARL changed to false -->
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-pmd-plugin</artifactId>
                <version>3.8</version>
                <configuration>
                    <printFailingErrors>true</printFailingErrors>
                    <!--<rulesets> -->
                    <!--<ruleset>example_pmd.xml</ruleset> -->
                    <!--</rulesets> -->
                    <excludeRoots>
                        <excludeRoot>target/generated-sources</excludeRoot>
                    </excludeRoots>
                </configuration>
                <executions>
                    <execution>
                        <goals>
                            <goal>check</goal>
                            <goal>cpd-check</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>cobertura-maven-plugin</artifactId>
                <version>2.7</version>
                <dependencies>
                    <dependency>
                        <groupId>org.ow2.asm</groupId>
                        <artifactId>asm</artifactId>
                        <version>5.0.3</version>
                    </dependency>
                    <dependency>
                        <groupId>net.sourceforge.cobertura</groupId>
                        <artifactId>cobertura-runtime</artifactId>
                        <version>2.1.1</version>
                        <type>pom</type>
                        <exclusions>
                            <exclusion>
                                <groupId>ch.qos.logback</groupId>
                                <artifactId>logback-classic</artifactId>
                            </exclusion>
                        </exclusions>
                    </dependency>
                    <dependency>
                        <groupId>net.sourceforge.cobertura</groupId>
                        <artifactId>cobertura</artifactId>
                        <version>2.1.1</version>
                        <exclusions>
                            <exclusion>
                                <groupId>ch.qos.logback</groupId>
                                <artifactId>logback-classic</artifactId>
                            </exclusion>
                        </exclusions>
                    </dependency>
                </dependencies>
                <configuration>
                    <!--<instrumentedDirectory>${basedir}/target/cobertura/instrumented-classes</instrumentedDirectory> -->
                    <outputDirectory>${basedir}/target/cobertura/report</outputDirectory>
                    <check>
                        <haltOnFailure>true</haltOnFailure>
                        <branchRate>85</branchRate>
                        <lineRate>85</lineRate>
                        <totalBranchRate>85</totalBranchRate>
                        <totalLineRate>85</totalLineRate>
                        <packageLineRate>85</packageLineRate>
                        <packageBranchRate>85</packageBranchRate>
                    </check>
                    <formats>
                        <format>xml</format>
                        <format>html</format>
                    </formats>
                    <instrumentation>
                        <excludes>
                            <exclude>**/generated/**/*</exclude>
                        </excludes>
                    </instrumentation>
                </configuration>
                <executions>
                    <execution>
                        <!-- don't run -->
                        <phase>none</phase>
                        <!-- <phase>package</phase> -->
                        <goals>
                            <goal>cobertura</goal>
                        </goals>
                    </execution>
                    <!-- <execution> -->
                    <!-- <id>clean</id> -->
                    <!-- <phase>clean</phase> -->
                    <!-- <goals> -->
                    <!-- <goal>clean</goal> -->
                    <!-- </goals> -->
                    <!-- </execution> -->
                    <!-- <execution> -->
                    <!-- <id>instrument</id> -->
                    <!-- <phase>package</phase> -->
                    <!-- <goals> -->
                    <!-- <goal>instrument</goal> -->
                    <!-- </goals> -->
                    <!-- </execution> -->
                    <!-- <execution> -->
                    <!-- <id>package</id> -->
                    <!-- <phase>package</phase> -->
                    <!-- <goals> -->
                    <!-- <goal>check</goal> -->
                    <!-- </goals> -->
                    <!-- </execution> -->
                </executions>
            </plugin>
            <!-- <plugin> -->
            <!-- <groupId>org.apache.maven.plugins</groupId> -->
            <!-- <artifactId>maven-jar-plugin</artifactId> -->
            <!-- <version>${maven.jar.plugin.version}</version> -->
            <!-- <executions> -->
            <!-- <execution> -->
            <!-- <phase>package</phase> -->
            <!-- <goals> -->
            <!-- <goal>jar</goal> -->
            <!-- </goals> -->
            <!-- <configuration> -->
            <!-- <classifier>client</classifier> -->
            <!-- <includes> -->
            <!-- <include>**/demo/api/**</include> -->
            <!-- <include>**/demo/config/**</include> -->
            <!-- </includes> -->
            <!-- </configuration> -->
            <!-- </execution> -->
            <!-- </executions> -->
            <!-- </plugin> -->
            <plugin>
                <groupId>org.jacoco</groupId>
                <artifactId>jacoco-maven-plugin</artifactId>
                <version>${jacoco.version}</version>
                <executions>
                    <execution>
                        <id>prepare-agent</id>
                        <goals>
                            <goal>prepare-agent</goal>
                        </goals>
                    </execution>
                    <execution>
                        <id>report</id>
                        <phase>prepare-package</phase>
                        <goals>
                            <goal>report</goal>
                        </goals>
                    </execution>
                    <execution>
                        <id>post-unit-test</id>
                        <phase>test</phase>
                        <goals>
                            <goal>report</goal>
                        </goals>
                        <configuration>
                            <!-- Sets the path to the file which contains the execution data. -->

                            <dataFile>target/jacoco.exec</dataFile>
                            <!-- Sets the output directory for the code coverage report. -->
                            <outputDirectory>target/jacoco-ut</outputDirectory>
                        </configuration>
                    </execution>
                </executions>
                <configuration>
                    <!--<systemPropertyVariables>
                        <jacoco-agent.destfile>target/jacoco.exec</jacoco-agent.destfile>
                    </systemPropertyVariables>-->
                </configuration>
            </plugin>
        </plugins>
    </build>
</project>