package com.ftd.partnerauthenticationservice.strategy;

import com.ftd.partnerauthenticationservice.api.domain.SiteId;
import com.ftd.partnerauthenticationservice.api.domain.request.PartnerAuthenticationRequest;
import com.ftd.partnerauthenticationservice.api.domain.response.PartnerAuthenticationResponse;
import com.ftd.partnerauthenticationservice.service.PartnerAuthenticationServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;



@RunWith(SpringRunner.class)
@SpringBootTest
public class AuthorizeMemberTest {

    @Autowired
    private PartnerAuthenticationServiceImpl partnerAuthenticationServiceImpl;

    @Test
    public void authenticateMember_Authorized1() {

        PartnerAuthenticationRequest request = new PartnerAuthenticationRequest();

        //request.setApiKey("475e4f18-faa7-4c42-8204-56a842c5dfe6");
        request.setApiKey("d3b26194-a956-4ce1-8081-5ba3573b3d6f");
        request.setApplicationName("FloristOrderService");
        request.setMemberCode("90-9086AA");
        request.setUserName("909086AA");
        request.setPassword("gZ6WcmuC4>");

        PartnerAuthenticationResponse response =
                partnerAuthenticationServiceImpl.authenticateMember(request, SiteId.teleflora);

        Assert.assertTrue(response.isAuthorized());
    }

    @Test
    public void authenticateMember_InvalidAPIKey() {

        PartnerAuthenticationRequest request = new PartnerAuthenticationRequest();

        request.setApiKey("InvalidAPIKey");
        request.setApplicationName("FloristOrderService");
        request.setMemberCode("90-9086AA");
        request.setUserName("909086AA");
        request.setPassword("gZ6WcmuC4>");

        PartnerAuthenticationResponse response =
                partnerAuthenticationServiceImpl.authenticateMember(request, SiteId.teleflora);

        Assert.assertTrue(response.isAuthorized());
    }

    @Test
    public void authenticateMember_InvalidApplicationName() {

        PartnerAuthenticationRequest request = new PartnerAuthenticationRequest();

        request.setApiKey("475e4f18-faa7-4c42-8204-56a842c5dfe6");
        request.setApplicationName("FloristService");
        request.setMemberCode("22-2222AA");

        PartnerAuthenticationResponse response =
                partnerAuthenticationServiceImpl.authenticateMember(request, SiteId.teleflora);

        Assert.assertFalse(response.isAuthorized());
    }

    @Test
    public void authenticateMember_InvalidMemberCode() {

        PartnerAuthenticationRequest request = new PartnerAuthenticationRequest();

        request.setApiKey("475e4f18-faa7-4c42-8204-56a842c5dfe6");
        request.setApplicationName("FloristOrderService");
        request.setMemberCode("22-2222ZZ");

        PartnerAuthenticationResponse response =
                partnerAuthenticationServiceImpl.authenticateMember(request, SiteId.teleflora);

        Assert.assertFalse(response.isAuthorized());
    }

    @Test
    public void authenticateMemberFallbackTest(){
        AuthorizeMember member = new AuthorizeMember();
        PartnerAuthenticationResponse response = member.authenticateMemberFallback(new PartnerAuthenticationRequest(),
                SiteId.teleflora);
        Assert.assertFalse(response.isAuthorized());
    }
}