package com.ftd.partnerauthenticationservice.strategy;

import com.ftd.partnerauthenticationservice.api.domain.SiteId;
import com.ftd.partnerauthenticationservice.api.domain.request.MemberCardOnFileRequest;
import com.ftd.partnerauthenticationservice.api.domain.response.MemberCardOnFileResponse;
import com.ftd.partnerauthenticationservice.service.PartnerAuthenticationServiceImpl;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GetMemberCOFCredentialsTest {


    private MemberCardOnFileRequest memberCardOnFileRequest = new MemberCardOnFileRequest();

    @Autowired
    private PartnerAuthenticationServiceImpl partnerAuthenticationServiceImpl;

    @Ignore
    @Test
    public void retrieveMemberCOFCredentials_InvalidApplicationName1() throws Exception {

        memberCardOnFileRequest.setMemberCode("33-3333AA");
        memberCardOnFileRequest.setApplicationName("CardOnFileTEST");

        MemberCardOnFileResponse memberCardOnFileResponse
                = partnerAuthenticationServiceImpl.retrieveMemberCOFCredentials(memberCardOnFileRequest, SiteId.teleflora);

        Assert.assertFalse(memberCardOnFileResponse.isAuthorized());
    }

    @Ignore
    @Test
    public void retrieveMemberCOFCredentials_InvalidApplicationName2() throws Exception {

        memberCardOnFileRequest.setMemberCode("33-3333AA");
        memberCardOnFileRequest.setApplicationName(null);

        MemberCardOnFileResponse memberCardOnFileResponse
                = partnerAuthenticationServiceImpl.retrieveMemberCOFCredentials(memberCardOnFileRequest, SiteId.teleflora);

        Assert.assertFalse(memberCardOnFileResponse.isAuthorized());
    }

    @Ignore
    @Test
    public void retrieveMemberCOFCredentials_InvalidMemberCode1() throws Exception {

        memberCardOnFileRequest.setMemberCode("33-3333");
        memberCardOnFileRequest.setApplicationName("CardOnFile");

        MemberCardOnFileResponse memberCardOnFileResponse
                = partnerAuthenticationServiceImpl.retrieveMemberCOFCredentials(memberCardOnFileRequest, SiteId.teleflora);

        Assert.assertFalse(memberCardOnFileResponse.isAuthorized());
    }

    @Ignore
    @Test
    public void retrieveMemberCOFCredentials_InvalidMemberCode2() throws Exception {

        memberCardOnFileRequest.setMemberCode("33-3331AA");
        memberCardOnFileRequest.setApplicationName("CardOnFile");

        MemberCardOnFileResponse memberCardOnFileResponse
                = partnerAuthenticationServiceImpl.retrieveMemberCOFCredentials(memberCardOnFileRequest, SiteId.teleflora);

        Assert.assertFalse(memberCardOnFileResponse.isAuthorized());
    }


    public void retrieveMemberCOFCredentials_InvalidSiteID1() throws Exception {

        memberCardOnFileRequest.setMemberCode("33-3333AA");
        memberCardOnFileRequest.setApplicationName("CardOnFile");

        MemberCardOnFileResponse memberCardOnFileResponse
                = partnerAuthenticationServiceImpl.retrieveMemberCOFCredentials(memberCardOnFileRequest, null);

        Assert.assertFalse(memberCardOnFileResponse.isAuthorized());
    }


    public void retrieveMemberCOFCredentials_InvalidSiteID2() throws Exception {

        memberCardOnFileRequest.setMemberCode("33-3333AA");
        memberCardOnFileRequest.setApplicationName("CardOnFile");

        MemberCardOnFileResponse memberCardOnFileResponse
                = partnerAuthenticationServiceImpl.retrieveMemberCOFCredentials(memberCardOnFileRequest, SiteId.ftd);

        Assert.assertFalse(memberCardOnFileResponse.isAuthorized());
    }

    @Test
    public void retrieveMemberCOFCredentialsFallbackTest(){
        GetMemberCOFCredentials memberCof = new GetMemberCOFCredentials();
        MemberCardOnFileResponse response = memberCof.retrieveMemberCOFCredentialsFallback(new MemberCardOnFileRequest()
                , SiteId.teleflora);
        Assert.assertFalse(response.isAuthorized());
    }

}