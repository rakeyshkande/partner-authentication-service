package com.ftd.partnerauthenticationservice.strategy;

import com.ftd.partnerauthenticationservice.api.domain.SiteId;
import com.ftd.partnerauthenticationservice.api.domain.request.PartnerAuthenticationRequest;
import com.ftd.partnerauthenticationservice.service.PartnerAuthenticationServiceImpl;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@SpringBootTest
public class ValidateAPIKeyTest {

    @Autowired
    private PartnerAuthenticationServiceImpl partnerAuthenticationServiceImpl;

    @Ignore
    @Test
    public void validAPIKey_Valid() {

        PartnerAuthenticationRequest request = new PartnerAuthenticationRequest();

        //request.setApiKey("475e4f18-faa7-4c42-8204-56a842c5dfe6");
        request.setApiKey("d3b26194-a956-4ce1-8081-5ba3573b3d6f");
        request.setApplicationName("FloristOrderService");

        Boolean isItValid = partnerAuthenticationServiceImpl.validAPIKey(request, SiteId.teleflora);

        Assert.assertTrue(isItValid);
    }

    @Test
    public void validAPIKey_InValid1() {

        PartnerAuthenticationRequest request = new PartnerAuthenticationRequest();

        request.setApiKey("123456789");

        request.setApplicationName("FloristOrderService");

        Boolean isItValid = partnerAuthenticationServiceImpl.validAPIKey(request, SiteId.teleflora);

        Assert.assertFalse(isItValid);
    }

    @Test
    public void validAPIKey_InValid2() {

        PartnerAuthenticationRequest request = new PartnerAuthenticationRequest();

        request.setApiKey("123456789");

        Boolean isItValid = partnerAuthenticationServiceImpl.validAPIKey(request, SiteId.teleflora);

        Assert.assertFalse(isItValid);
    }

    @Test
    public void validApiKeyFallbackTest(){
        ValidateAPIKey validateAPIKey = new ValidateAPIKey();
        Boolean isValid = validateAPIKey.validApiKeyFallback("FOS", "Test",
                "90-9085CV", "Test User",SiteId.teleflora);

        Assert.assertFalse(isValid);
    }

}