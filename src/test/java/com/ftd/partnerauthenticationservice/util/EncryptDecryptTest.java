package com.ftd.partnerauthenticationservice.util;


import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;


@RunWith(SpringRunner.class)
@SpringBootTest
public class EncryptDecryptTest {


    @Autowired
    private EncryptDecrypt encryptDecrypt;

    @Ignore
    @Test
    public void testEncryptDecrypt1() throws Exception {

        String testString = "THis is a test";

        String encryptedString = encryptDecrypt.encrypt(testString);

        String decryptedString = encryptDecrypt.decrypt(encryptedString);

        assertTrue(testString.equals(decryptedString));
    }

    @Ignore
    @Test
    public void doubleEncryptDecrypt() throws Exception {

        String testString = "THis is a test";

        String encryptedString = encryptDecrypt.encrypt(testString);

        encryptedString = encryptDecrypt.encrypt(encryptedString);

        String decryptedString = encryptDecrypt.decrypt(encryptedString);

        decryptedString = encryptDecrypt.decrypt(decryptedString);

        assertTrue(testString.equals(decryptedString));

    }

    @Ignore
    @Test
    public void decrypt() {
    }



}