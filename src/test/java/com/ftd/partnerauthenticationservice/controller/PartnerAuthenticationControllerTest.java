package com.ftd.partnerauthenticationservice.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ftd.commons.misc.exception.InputValidationException;
import com.ftd.partnerauthenticationservice.api.domain.SiteId;
import com.ftd.partnerauthenticationservice.api.domain.request.MemberCardOnFileRequest;
import com.ftd.partnerauthenticationservice.api.domain.request.PartnerAuthenticationRequest;
import com.ftd.partnerauthenticationservice.api.domain.response.MemberCardOnFileResponse;
import com.ftd.partnerauthenticationservice.api.domain.response.PartnerAuthenticationResponse;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;


@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class PartnerAuthenticationControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private PartnerAuthenticationController partnerAuthenticationController;

    private PartnerAuthenticationRequest partnerAuthenticationRequest;

    private MemberCardOnFileRequest memberCardOnFileRequest;

    @Before
    public void setUp() throws Exception {
        this.partnerAuthenticationRequest = new PartnerAuthenticationRequest();

        this.memberCardOnFileRequest = new MemberCardOnFileRequest();

    }

    @After
    public void tearDown() throws Exception {
    }

    public static String asJsonString(Object obj) {
        try {
            return (new ObjectMapper()).writeValueAsString(obj);
        } catch (Exception var2) {
            throw new RuntimeException(var2);
        }
    }

    @Ignore
    @Test
    public void authenticateMember_Post1() throws Exception {

        PartnerAuthenticationRequest partnerAuthenticationRequest = new PartnerAuthenticationRequest();
        partnerAuthenticationRequest.setMemberCode("22-2222AA");
        partnerAuthenticationRequest.setApiKey("475e4f18-faa7-4c42-8204-56a842c5dfe6");
        partnerAuthenticationRequest.setApplicationName("FloristOrderService");
        partnerAuthenticationRequest.setUserName("testUser");
        partnerAuthenticationRequest.setPassword("testPassword");

        this.partnerAuthenticationController.authenticateMember(partnerAuthenticationRequest, SiteId.teleflora);
        this.mockMvc.perform(MockMvcRequestBuilders.post("/teleflora/api/authenticateMember", new Object[0]).contentType(MediaType.APPLICATION_JSON).content(asJsonString(partnerAuthenticationRequest))).andExpect(MockMvcResultMatchers.status().isOk());

    }

    @Ignore
    @Test
    public void authenticateMember1() throws Exception {
        this.partnerAuthenticationRequest.setMemberCode("11-1111AC");
        this.partnerAuthenticationRequest.setApiKey("475e4f18-faa7-4c42-8204-56a842c5dfe6");
        this.partnerAuthenticationRequest.setApplicationName("FloristOrderService");
        this.partnerAuthenticationRequest.setUserName("testUser");
        this.partnerAuthenticationRequest.setPassword("testPassword");
        ResponseEntity<PartnerAuthenticationResponse> response = this.partnerAuthenticationController.authenticateMember(this.partnerAuthenticationRequest, SiteId.teleflora);
        Assert.assertEquals(response.getStatusCode(), HttpStatus.OK);
    }

    @Ignore
    @Test
    public void authenticateMember2() throws Exception {
        this.partnerAuthenticationRequest.setMemberCode("90-9085AC");
        this.partnerAuthenticationRequest.setApiKey("475e4f18-faa7-4c42-8204-56a842c5dfe6");
        this.partnerAuthenticationRequest.setApplicationName("FloristOrderService");
        this.partnerAuthenticationRequest.setUserName("testUser");
        this.partnerAuthenticationRequest.setPassword("testPassword");
        ResponseEntity<PartnerAuthenticationResponse> response = this.partnerAuthenticationController.authenticateMember(this.partnerAuthenticationRequest, SiteId.teleflora);
        Assert.assertEquals(response.getStatusCode(), HttpStatus.OK);
    }

    @Test(
            expected = InputValidationException.class
    )
    public void authenticateMember_NULLRequest() throws Exception {
        this.partnerAuthenticationRequest.setMemberCode(null);
        this.partnerAuthenticationRequest.setApiKey(null);
        this.partnerAuthenticationRequest.setApplicationName(null);
        ResponseEntity<PartnerAuthenticationResponse> response = this.partnerAuthenticationController.authenticateMember(null, SiteId.teleflora);
        Assert.assertEquals(response.getStatusCode(), HttpStatus.BAD_REQUEST);
    }

    @Test(
            expected = InputValidationException.class
    )
    public void authenticateMember_EmptyRequest() throws Exception {
        this.partnerAuthenticationRequest.setMemberCode(null);
        this.partnerAuthenticationRequest.setApiKey(null);
        this.partnerAuthenticationRequest.setApplicationName(null);
        ResponseEntity<PartnerAuthenticationResponse> response = this.partnerAuthenticationController.authenticateMember(this.partnerAuthenticationRequest, SiteId.teleflora);
        Assert.assertEquals(response.getStatusCode(), HttpStatus.BAD_REQUEST);
    }

    @Test(
            expected = InputValidationException.class
    )
    public void authenticateMember_InvalidMember() throws Exception {
        this.partnerAuthenticationRequest.setMemberCode("xx-xxxxaa");
        this.partnerAuthenticationRequest.setApiKey("1234567890987654321");
        this.partnerAuthenticationRequest.setApplicationName("FloristOrderService");
        ResponseEntity<PartnerAuthenticationResponse> response = this.partnerAuthenticationController.authenticateMember(this.partnerAuthenticationRequest, SiteId.teleflora);
        Assert.assertEquals(response.getStatusCode(), HttpStatus.BAD_REQUEST);
    }

    @Test(
            expected = InputValidationException.class
    )
    public void authenticateMember_MissingAPIKey() throws Exception {
        this.partnerAuthenticationRequest.setMemberCode("11-1111AA");
        this.partnerAuthenticationRequest.setApiKey(null);
        this.partnerAuthenticationRequest.setApplicationName("FloristOrderService");
        ResponseEntity<PartnerAuthenticationResponse> response = this.partnerAuthenticationController.authenticateMember(this.partnerAuthenticationRequest, SiteId.teleflora);
        Assert.assertEquals(response.getStatusCode(), HttpStatus.BAD_REQUEST);
    }

    @Test(
            expected = InputValidationException.class
    )
    public void authenticateMember_MissingApplicationName() throws Exception {
        this.partnerAuthenticationRequest.setMemberCode("11-1111AA");
        this.partnerAuthenticationRequest.setApiKey("1234567890987654321");
        this.partnerAuthenticationRequest.setApplicationName(null);
        ResponseEntity<PartnerAuthenticationResponse> response = this.partnerAuthenticationController.authenticateMember(this.partnerAuthenticationRequest, SiteId.teleflora);
        Assert.assertEquals(response.getStatusCode(), HttpStatus.BAD_REQUEST);
    }

    @Test(
            expected = InputValidationException.class
    )
    public void authenticateMember_InvalidSiteID() throws Exception {
        this.partnerAuthenticationRequest.setMemberCode("11-1111AC");
        this.partnerAuthenticationRequest.setApiKey("1234567890987654321");
        this.partnerAuthenticationRequest.setApplicationName("FloristOrderService");
        ResponseEntity<PartnerAuthenticationResponse> response = this.partnerAuthenticationController.authenticateMember(this.partnerAuthenticationRequest, null);
        Assert.assertEquals(response.getStatusCode(), HttpStatus.BAD_REQUEST);
    }

    @Test(
            expected = InputValidationException.class
    )
    public void retrieveMemberCOFCredentials_InvalidMemberCode() throws Exception {

        this.memberCardOnFileRequest.setMemberCode("11-111aa");
        this.memberCardOnFileRequest.setApplicationName("CardOnFile");

        ResponseEntity<MemberCardOnFileResponse> response = this.partnerAuthenticationController.retrieveMemberCOFCredentials(this.memberCardOnFileRequest, SiteId.teleflora);
        Assert.assertEquals(response.getStatusCode(), HttpStatus.BAD_REQUEST);
    }

    @Test(
            expected = InputValidationException.class
    )
    public void retrieveMemberCOFCredentials_InvalidApplicationName1() throws Exception {

        this.memberCardOnFileRequest.setMemberCode("11-1111aa");
        this.memberCardOnFileRequest.setApplicationName(null);

        ResponseEntity<MemberCardOnFileResponse> response = this.partnerAuthenticationController.retrieveMemberCOFCredentials(this.memberCardOnFileRequest, SiteId.teleflora);
        Assert.assertEquals(response.getStatusCode(), HttpStatus.BAD_REQUEST);
    }

    @Test(
            expected = InputValidationException.class
    )
    public void retrieveMemberCOFCredentials_InvalidApplicationName2() throws Exception {

        this.memberCardOnFileRequest.setMemberCode("11-1111aa");
        this.memberCardOnFileRequest.setApplicationName("");

        ResponseEntity<MemberCardOnFileResponse> response = this.partnerAuthenticationController.retrieveMemberCOFCredentials(this.memberCardOnFileRequest, SiteId.teleflora);
        Assert.assertEquals(response.getStatusCode(), HttpStatus.BAD_REQUEST);
    }

    @Test(
            expected = InputValidationException.class
    )
    public void retrieveMemberCOFCredentials_InvalidSiteID1() throws Exception {

        this.memberCardOnFileRequest.setMemberCode("11-111aa");
        this.memberCardOnFileRequest.setApplicationName("CardOnFile");

        ResponseEntity<MemberCardOnFileResponse> response = this.partnerAuthenticationController.retrieveMemberCOFCredentials(this.memberCardOnFileRequest, null);
        Assert.assertEquals(response.getStatusCode(), HttpStatus.BAD_REQUEST);
    }

    @Test(
            expected = InputValidationException.class
    )
    public void retrieveMemberCOFCredentials_InvalidSiteID2() throws Exception {

        this.memberCardOnFileRequest.setMemberCode("11-111aa");
        this.memberCardOnFileRequest.setApplicationName("CardOnFile");

        ResponseEntity<MemberCardOnFileResponse> response = this.partnerAuthenticationController.retrieveMemberCOFCredentials(this.memberCardOnFileRequest, SiteId.interflora);
        Assert.assertEquals(response.getStatusCode(), HttpStatus.BAD_REQUEST);
    }


}