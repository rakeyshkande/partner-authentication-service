package com.ftd.partnerauthenticationservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
//import org.springframework.boot.autoconfigure.data.mongo.MongoDataAutoConfiguration;
//import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;

@EnableCircuitBreaker
@EnableHystrixDashboard
@SpringBootApplication(scanBasePackages = {"com.ftd.commons", "com.ftd.partnerauthenticationservice"})
//exclude = {MongoAutoConfiguration.class, MongoDataAutoConfiguration.class})
public class PartnerAuthenticationServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(PartnerAuthenticationServiceApplication.class, args);
    }


}
