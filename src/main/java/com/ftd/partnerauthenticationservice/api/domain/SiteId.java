package com.ftd.partnerauthenticationservice.api.domain;

public enum SiteId {
    ftd, teleflora, proflowers, berries, interflora, personalcreations, gifts, fol, pfol
}
