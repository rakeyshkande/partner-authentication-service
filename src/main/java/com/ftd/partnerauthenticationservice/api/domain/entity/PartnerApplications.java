package com.ftd.partnerauthenticationservice.api.domain.entity;


import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class PartnerApplications {

    private String name;
    private List<PartnerMember> member;

    public PartnerApplications(String name,
                               List<PartnerMember> member) {
        this.name = name;
        this.member = member;
    }
}



