package com.ftd.partnerauthenticationservice.api.domain.request;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import io.swagger.annotations.ApiModelProperty;

@JsonRootName("MemberCardOnFileRequest")
public class MemberCardOnFileRequest {


    @ApiModelProperty(notes = "Partner's apiKey is mandatory field.")
    @JsonProperty("apiKey")
    private String apiKey;

    @ApiModelProperty(notes = "applicationName is mandatory field.")
    @JsonProperty("applicationName")
    private String applicationName;

    @ApiModelProperty(notes = "Membercode is mandatory field.")
    @JsonProperty("memberCode")
    private String memberCode;

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getApplicationName() {
        return applicationName;
    }

    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }

    public String getMemberCode() {
        return memberCode;
    }

    public void setMemberCode(String memberCode) {
        this.memberCode = memberCode;
    }

    @Override
    public String toString() {
        return "MemberCardOnFileRequest{" +
                "applicationName='" + applicationName + '\'' +
                "memberCode='" + memberCode + '\'' +
                '}';
    }


}
