package com.ftd.partnerauthenticationservice.api.domain.entity;


import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class PartnerMember {

    private String code;
    private Long groupId;
    private String userName;
    private String password;
    private Boolean active;
    private LocalDateTime updatedOn;
    private String updatedBy;
    private LocalDateTime createdOn;
    private String createdBy;

    public PartnerMember(String code,
                         Long groupId,
                         String userName,
                         String password,
                         Boolean active,
                         LocalDateTime updatedOn,
                         String updatedBy,
                         LocalDateTime createdOn,
                         String createdBy) {
        this.code = code;
        this.groupId = groupId;
        this.userName = userName;
        this.password = password;
        this.active = active;
        this.updatedOn = updatedOn;
        this.updatedBy = updatedBy;
        this.createdOn = createdOn;
        this.createdBy = createdBy;
    }
}
