package com.ftd.partnerauthenticationservice.api.domain.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import io.swagger.annotations.ApiModelProperty;

@JsonRootName("PartnerAuthenticationRequest")
public class PartnerAuthenticationRequest {

    @ApiModelProperty(notes = "Partner's apiKey is mandatory field.")
    @JsonProperty("apiKey")
    private String apiKey;

    @ApiModelProperty(notes = "applicationName is mandatory field.")
    @JsonProperty("applicationName")
    private String applicationName;

    @ApiModelProperty(notes = "memberCode is mandatory field.")
    @JsonProperty("memberCode")
    private String memberCode;

    @ApiModelProperty(notes = "userName is mandatory field.")
    @JsonProperty("userName")
    private String userName;

    @ApiModelProperty(notes = "password is mandatory field.")
    @JsonProperty("password")
    private String password;

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getApplicationName() {
        return applicationName;
    }

    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }

    public String getMemberCode() {
        return memberCode;
    }

    public void setMemberCode(String memberCode) {
        this.memberCode = memberCode;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "PartnerAuthenticationRequest{" +
                "apiKey='" + apiKey + '\'' +
                "applicationName='" + applicationName + '\'' +
                "memberCode='" + memberCode + '\'' +
                '}';
    }


}
