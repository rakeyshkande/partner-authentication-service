package com.ftd.partnerauthenticationservice.api.domain.response;


import com.fasterxml.jackson.annotation.JsonRootName;

@JsonRootName("MemberCardOnFileResponse")
public class MemberCardOnFileResponse {

    private String memberCode;

    private boolean authorized;

    private long groupId;

    private String userName;

    private String password;

    public String getMemberCode() {
        return memberCode;
    }

    public void setMemberCode(String memberCode) {
        this.memberCode = memberCode;
    }

    public boolean isAuthorized() {
        return authorized;
    }

    public void setAuthorized(boolean authorized) {
        this.authorized = authorized;
    }

    public long getGroupId() {
        return groupId;
    }

    public void setGroupId(long groupId) {
        this.groupId = groupId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    @Override
    public String toString() {
        return "MemberCardOnFileResponse{" +
                "memberCode='" + memberCode + '\'' +
                ", authorized='" + authorized + '\'' +
                ", groupId='" + groupId + '\'' +
                ", userName='" + userName + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
 }
