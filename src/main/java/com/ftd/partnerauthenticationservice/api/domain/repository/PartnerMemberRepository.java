package com.ftd.partnerauthenticationservice.api.domain.repository;

import com.ftd.partnerauthenticationservice.api.domain.entity.PartnerMember;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface PartnerMemberRepository extends MongoRepository<PartnerMember, String> {


}
