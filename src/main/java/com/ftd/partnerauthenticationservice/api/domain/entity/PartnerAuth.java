package com.ftd.partnerauthenticationservice.api.domain.entity;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@ToString(exclude = "id")
@Document(collection = "partnerAuths")

public class PartnerAuth {
    @Id
    private String id;
    private String partnerName;
    private String apiKey;
    private String applicationName;
    private boolean active;
    private LocalDateTime createdOn;
    private String createdBy;
    private LocalDateTime updatedOn;
    private String updatedBy;
    private List<PartnerMember> members;

    public PartnerAuth(String partnerName,
                       String apiKey,
                       String applicationName,
                       boolean active,
                       LocalDateTime createdOn,
                       String createdBy,
                       LocalDateTime updatedOn,
                       String updatedBy,
                       List<PartnerMember> members) {

        this.partnerName = partnerName;
        this.apiKey = apiKey;
        this.applicationName = applicationName;
        this.active = active;
        this.createdOn = createdOn;
        this.createdBy = createdBy;
        this.updatedOn = updatedOn;
        this.updatedBy = updatedBy;
        this.members = members;
    }

    /*
        @Id
    private String id;
    private String partnerName;
    private String apiKey;
    private boolean active;
    private LocalDateTime createdOn;
    private String createdBy;
    private LocalDateTime updatedOn;
    private String updatedBy;
    private List<PartnerApplications> applications;


    public PartnerAuth(String partnerName,
                       String apiKey,
                       boolean active,
                       LocalDateTime createdOn,
                       String createdBy,
                       LocalDateTime updatedOn,
                       String updatedBy,
                       List<PartnerApplications> applications) {

            this.partnerName = partnerName;
            this.apiKey = apiKey;
            this.active = active;
            this.createdOn = createdOn;
            this.createdBy = createdBy;
            this.updatedOn = updatedOn;
            this.updatedBy = updatedBy;
            this.applications = applications;
    }
     */
}
