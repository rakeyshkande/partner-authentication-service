package com.ftd.partnerauthenticationservice.api.domain.constants;

import org.springframework.stereotype.Component;

@Component
public final class GlobalConstants {


    public static final int SUCCESS = 200;
    public static final int CLIENT_ERROR = 400;
    public static final int SERVER_ERROR = 500;
    public static final String EMPTYREQUEST = "Empty Request";
    public static final String EMPTYREQUESTERROR = "Empty Request not allowed";
    public static final String EMPTYREQUESTEXCEPTION = "Empty Request. Exception will be thrown";
    public static final String SERVICEUNAVAILABLE = "Service is not available";

    public static final String AUTHENTICATIONSUCCESS = "BLAHBLAHBLAH";

    public static final String INVALIDREQUEST = "Invalid Request";
    public static final String EMPTYORBADREQUEST = "Empty or Bad Request";
    public static final String EMPTYORBADREQUEST_USERNAMEPASSWORD = "Empty or Bad Request - UserName/Password";
    public static final String INVALIDAPIKEY = "Invalid API Key";

}
