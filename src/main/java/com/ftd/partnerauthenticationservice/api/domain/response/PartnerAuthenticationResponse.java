package com.ftd.partnerauthenticationservice.api.domain.response;

public class PartnerAuthenticationResponse {

    //private String memberCode;

    private boolean authorized;
/*
    public String getMemberCode() {
        return memberCode;
    }

    public void setMemberCode(String memberCode) {
        this.memberCode = memberCode;
    }
*/
    public boolean isAuthorized() {
        return authorized;
    }

    public void setAuthorized(boolean authorized) {
        this.authorized = authorized;
    }


    @Override
    public String toString() {
        return "PartnerAuthenticationResponse{" +
               // "memberCode='" + memberCode + '\'' +
                ", authorized='" + authorized + '\'' +
                '}';
    }

}
