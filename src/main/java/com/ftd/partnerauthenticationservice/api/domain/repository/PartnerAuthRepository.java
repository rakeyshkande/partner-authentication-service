package com.ftd.partnerauthenticationservice.api.domain.repository;

import com.ftd.partnerauthenticationservice.api.domain.entity.PartnerAuth;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

public interface PartnerAuthRepository extends MongoRepository<PartnerAuth, String> {

    @Query("{ 'partnerName': ?0," +
            " 'applicationName': ?1," +
            " 'active': ?2 }")
    List<PartnerAuth> getAPIKey(String partnerName,
                                String applicationName,
                                Boolean activePartner);


    @Query("{ 'partnerName': ?0," +
            " 'applicationName': ?1," +
            " 'active': ?2," +
            " 'members': {$elemMatch: { \"code\": ?3," +
            " \"active\": ?4 }}}")
    List<PartnerAuth> authenticateMember(String partnerName,
                                         String applicationName,
                                         Boolean activePartner,
                                         String applicationMemberCode,
                                         Boolean applicationMemberActive);

    @Query("{ 'partnerName': ?0," +
            " 'applicationName': ?1," +
            " 'active': ?2," +
            " 'members': {$elemMatch: { \"code\": ?3," +
            " \"userName\": ?4," +
            //" \"password\": ?5," +
            " \"active\": ?5 }}}")
    List<PartnerAuth> authenticateMemberUserPassword(String partnerName,
                                                     String applicationName,
                                                     Boolean activePartner,
                                                     String applicationMemberCode,
                                                     String userName,
                                                     //String password,
                                                     Boolean applicationMemberActive);

    @Query("{ 'partnerName': ?0," +
            " 'applicationName': ?1," +
            " 'active': ?2," +
            " 'members': {$elemMatch: { \"code\": ?3," +
            " \"active\": ?4 }}}")
    List<PartnerAuth> getMemberCOFCredentials(String partnerName,
                                            String applicationName,
                                            Boolean activePartner,
                                            String applicationMemberCode,
                                            Boolean applicationMemberActive);

/*
    @Query("{ 'partnerName': ?0," +
            " 'active': ?1 }")
    List<PartnerAuth> getAPIKey(String partnerName,
                                Boolean activePartner);

    @Query("{ 'partnerName': ?0," +
            " 'active': ?1," +
            " 'applications' : {$elemMatch: { \"name\": ?2," +
            " \"member\": {$elemMatch: { \"code\": ?3," +
            " \"active\": ?4 }}}}}")
    List<PartnerAuth> authenticateMember(String partnerName,
                                         Boolean activePartner,
                                         String applicationName,
                                         String applicationMemberCode,
                                         Boolean applicationMemberActive);

    @Query("{ 'partnerName': ?0," +
            " 'active': ?1," +
            " 'applications' : {$elemMatch: { \"name\": ?2," +
            " \"member\": {$elemMatch: { \"code\": ?3," +
            " \"userName\": ?4," +
            " \"password\": ?5," +
            " \"active\": ?6 }}}}}")
    List<PartnerAuth> authenticateMemberUserPassword(String partnerName,
                                                 Boolean activePartner,
                                                 String applicationName,
                                                 String applicationMemberCode,
                                                 String userName,
                                                 String password,
                                                 Boolean applicationMemberActive);
    @Query("{ 'partnerName': ?0," +
            " 'active': ?1," +
            " 'applications' : {$elemMatch: { \"name\": ?2," +
            " \"member\": {$elemMatch: { \"code\": ?3," +
            " \"active\": ?4 }}}}}")
    List<PartnerAuth> getMemberCOFCredentials(String partnerName,
                                             Boolean activePartner,
                                             String applicationName,
                                             String applicationMemberCode,
                                             Boolean applicationMemberActive);

    @Query("{ 'partnerName': ?0," +
            " 'apiKey': ?1," +
            " 'active': ?2 }")
    List<PartnerAuth> validateAPIKey(String partnerName,
                                     String apiKey,
                                     Boolean activePartner);
*/
/*

    @Query("{ 'partnerName': ?0," +
            " 'apiKey': ?1," +
            " 'active': ?2," +
            " 'applications' : {$elemMatch: { \"name\": ?3," +
            " \"member\": {$elemMatch: { \"code\": ?4," +
            " \"active\": ?5 }}}}}")
    List<PartnerAuth> authenticateMember(String partnerName,
                                         String apiKey,
                                         Boolean activePartner,
                                         String applicationName,
                                         String applicationMemberCode,
                                         Boolean applicationMemberActive);

    @Query("{ 'partnerName': ?0," +
            " 'apiKey': ?1," +
            " 'active': ?2," +
            " 'applications.name': ?3," +
            " 'applications.member.code': ?4," +
            " 'applications.member.active': ?5 }")
    List<PartnerAuth> authenticateMemberSAVE(String partnerName,
                                             String apiKey,
                                             Boolean activePartner,
                                             String applicationName,
                                             String applicationMemberCode,
                                             Boolean applicationMemberActive);

    @Query("{ 'partnerName': ?0," +
            " 'apiKey': ?1," +
            " 'active': ?2," +
            " 'applications.name': ?3 }")
    List<PartnerAuth> authenticateMemberTest(String partnerName,
                                             String apiKey,
                                             Boolean activePartner,
                                             String applicationName);

    @Query("{ 'partnerName': ?0," +
            " 'apiKey': ?1," +
            " 'active': ?2," +
            " 'applications.name': ?3," +
            " 'applications.member.code': ?4," +
            " 'applications.member.active': ?5 }")
    Boolean authenticateMemberBool(String partnerName,
                                   String apiKey,
                                   Boolean activePartner,
                                   String applicationName,
                                   String applicationMemberCode,
                                   Boolean applicationMemberActive);


    // @Query("{ 'partnerName': ?0 }")
    List<PartnerAuth> findAllByActive(Boolean active);

*/
}
