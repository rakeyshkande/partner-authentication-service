package com.ftd.partnerauthenticationservice.api.domain.repository;

import com.ftd.partnerauthenticationservice.api.domain.entity.PartnerApplications;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface PartnerApplicaitonRepository extends MongoRepository<PartnerApplications, String> {
}
