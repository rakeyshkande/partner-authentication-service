package com.ftd.partnerauthenticationservice.controller;

import com.codahale.metrics.annotation.ExceptionMetered;
import com.codahale.metrics.annotation.Timed;
import com.ftd.commons.logging.annotation.LogExecutionTime;
import com.ftd.commons.misc.exception.InputValidationException;
import com.ftd.partnerauthenticationservice.api.domain.constants.GlobalConstants;
import com.ftd.partnerauthenticationservice.api.domain.SiteId;
import com.ftd.partnerauthenticationservice.api.domain.request.PartnerAuthenticationRequest;
import com.ftd.partnerauthenticationservice.api.domain.response.PartnerAuthenticationResponse;
import com.ftd.partnerauthenticationservice.api.domain.request.MemberCardOnFileRequest;
import com.ftd.partnerauthenticationservice.api.domain.response.MemberCardOnFileResponse;
import com.ftd.partnerauthenticationservice.service.PartnerAuthenticationServiceImpl;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping(value = "{siteId}/api")
public class PartnerAuthenticationController {

    private static final Logger LOGGER = LoggerFactory.getLogger(PartnerAuthenticationController.class);

    @Autowired
    private PartnerAuthenticationServiceImpl authenticationImpl;

 //   @Value("${validation.validateCredentials}")
 //   private boolean validateCredentials;

    @ApiOperation(value = "Authenticate Member Code",
            notes = "Authenticate of Partner Member Code. This service expects that user must " +
                    "provide the Partner API Key, Application Name and the Partner's Member Code.")
    @ApiResponses(value = {
            @ApiResponse(code = GlobalConstants.SUCCESS, message = GlobalConstants.AUTHENTICATIONSUCCESS),
            @ApiResponse(code = GlobalConstants.CLIENT_ERROR, message = GlobalConstants.INVALIDREQUEST),
            @ApiResponse(code = GlobalConstants.SERVER_ERROR, message = GlobalConstants.SERVICEUNAVAILABLE)})

    @Timed
    @ExceptionMetered
    @LogExecutionTime
    @RequestMapping(value = "/authenticateMember", method = RequestMethod.POST)


    public ResponseEntity<PartnerAuthenticationResponse> authenticateMember(

            //public boolean authenticateMember(
            @RequestBody(required = false) PartnerAuthenticationRequest request,
            @ApiParam(name = "siteId", required = true, value = "The siteId where sale was generated",
                    allowableValues = "teleflora")
            @PathVariable(value = "siteId") final SiteId siteId
    ) {

        LOGGER.info("authenticateMember - received request from client. Validating Request. ");
// isBlank
        if (request == null ||
                siteId == null || StringUtils.isEmpty(siteId.toString()) ||
                StringUtils.isEmpty(request.getApiKey()) ||
                StringUtils.isEmpty(request.getApplicationName()) ||
                StringUtils.isEmpty(request.getMemberCode()) ||
                !request.getMemberCode().matches("\\d{2}-\\d{4}[a-zA-Z]{2}")) {

            LOGGER.error("error on authenticateMember request {}.", GlobalConstants.EMPTYREQUESTEXCEPTION);

            throw new InputValidationException(HttpStatus.BAD_REQUEST.name(),
                    GlobalConstants.EMPTYORBADREQUEST);
        }

// TODO 52 Don't validate the username password until second drop
        if (StringUtils.isEmpty(request.getUserName()) ||
                StringUtils.isEmpty(request.getPassword())) {
            LOGGER.error(GlobalConstants.EMPTYREQUESTEXCEPTION);
            throw new InputValidationException(HttpStatus.BAD_REQUEST.name(),
                    GlobalConstants.EMPTYORBADREQUEST_USERNAMEPASSWORD);
        }

        /*
        if (validateCredentials) {
            if (StringUtils.isEmpty(request.getUserName()) ||
                    StringUtils.isEmpty(request.getPassword())) {
                LOGGER.error(GlobalConstants.EMPTYREQUESTEXCEPTION);
                throw new InputValidationException(HttpStatus.BAD_REQUEST.name(),
                        GlobalConstants.EMPTYORBADREQUEST_USERNAMEPASSWORD);
            }
        } else {

            if (StringUtils.isEmpty(request.getUserName())) {
                request.setUserName("NotProvided.");
            }

            if (StringUtils.isEmpty(request.getPassword())) {
                request.setPassword("NotProvided.");
            }
        }
        */

        LOGGER.info("authenticateMember - received request from client - Member {}, Username {}",
                request.getMemberCode(), request.getUserName());

        PartnerAuthenticationResponse partnerAuthenticationResponse = new PartnerAuthenticationResponse();
        Boolean validAPIKey = false;

        try {

            //LOGGER.info("authenticateMember - transform the request to {}", request);
            //LOGGER.info("authenticateMember - request validated.");

            long startTime = System.currentTimeMillis();

            validAPIKey = authenticationImpl.validAPIKey(request, siteId);

            if (validAPIKey) {
                partnerAuthenticationResponse = authenticationImpl.authenticateMember(request, siteId);
            }

            LOGGER.info("authenticateMember - Response: {} ", partnerAuthenticationResponse);

            LOGGER.info("authenticateMember memberCode='{}' - completed in {} ms",
                    request.getMemberCode(), (System.currentTimeMillis() - startTime));

        } catch (Exception e) {
            LOGGER.error("authenticateMember Exception occurred! {}", e.getLocalizedMessage());
            throw new InputValidationException(HttpStatus.BAD_REQUEST.name(),
                    GlobalConstants.EMPTYORBADREQUEST);
        }

        if (!validAPIKey) {
            throw new InputValidationException(HttpStatus.UNAUTHORIZED.name(),
                    GlobalConstants.INVALIDAPIKEY);
        }

        return new ResponseEntity<>(partnerAuthenticationResponse, HttpStatus.OK);

        //return partnerAuthenticationResponse.isAuthorized();

    }


    @ApiOperation(value = "Retrieves given Members COF Credentials",
            notes = "Authenticate of Partner Member Code. This service expects that user must " +
                    "provide the Partner API Key, Application Name and the Partner's Member Code.")
    @ApiResponses(value = {
            @ApiResponse(code = GlobalConstants.SUCCESS, message = GlobalConstants.AUTHENTICATIONSUCCESS),
            @ApiResponse(code = GlobalConstants.CLIENT_ERROR, message = GlobalConstants.INVALIDREQUEST),
            @ApiResponse(code = GlobalConstants.SERVER_ERROR, message = GlobalConstants.SERVICEUNAVAILABLE)})

    @Timed
    @ExceptionMetered
    @LogExecutionTime
    @RequestMapping(value = "/retrieveMemberCOFCredentials", method = RequestMethod.POST)


    public ResponseEntity<MemberCardOnFileResponse> retrieveMemberCOFCredentials(

            @RequestBody(required = false) MemberCardOnFileRequest request,
            @ApiParam(name = "siteId", required = true, value = "The siteId where sale was generated",
                    allowableValues = "teleflora")
            @PathVariable(value = "siteId") final SiteId siteId
    ) {
        LOGGER.info("retrieveMemberCOFCredentials - received request from client. Validating Request. ");

        if (request == null ||
                siteId == null || StringUtils.isEmpty(siteId.toString()) ||
                StringUtils.isEmpty(request.getApiKey()) ||
                StringUtils.isEmpty(request.getApplicationName()) ||
                StringUtils.isEmpty(request.getMemberCode()) ||
                !request.getMemberCode().matches("\\d{2}-\\d{4}[a-zA-Z]{2}")) {

            LOGGER.error("error on authenticateMember request {}.", GlobalConstants.EMPTYREQUESTEXCEPTION);

            throw new InputValidationException(HttpStatus.BAD_REQUEST.name(),
                    GlobalConstants.EMPTYORBADREQUEST);
        }

        LOGGER.info("retrieveMemberCOFCredentials - received request from client - Member {}",
                request.getMemberCode());

        Boolean validAPIKey = false;
        MemberCardOnFileResponse memberCardOnFileResponse = new MemberCardOnFileResponse();

        try {

            long startTime = System.currentTimeMillis();

            validAPIKey = authenticationImpl.validAPIKey(request, siteId);

            if (validAPIKey) {

                memberCardOnFileResponse = authenticationImpl.retrieveMemberCOFCredentials(request, siteId);
            }

            LOGGER.info("retrieveMemberCOFCredentials - Response: memberCode='{}', authorized='{}', groupId='{}'," +
                            " userName='{}' ",
                    memberCardOnFileResponse.getMemberCode(),
                    memberCardOnFileResponse.isAuthorized(),
                    memberCardOnFileResponse.getGroupId(),
                    memberCardOnFileResponse.getUserName());

            LOGGER.info("retrieveMemberCOFCredentials memberCode='{}' - completed in {} ms",
                    memberCardOnFileResponse.getMemberCode(), (System.currentTimeMillis() - startTime));

        } catch (Exception e) {
            LOGGER.error("authenticateMember Exception occurred! {}", e.getLocalizedMessage());
            throw new InputValidationException(HttpStatus.BAD_REQUEST.name(),
                    GlobalConstants.EMPTYORBADREQUEST);
        }

        if (!validAPIKey) {
            throw new InputValidationException(HttpStatus.UNAUTHORIZED.name(),
                    GlobalConstants.INVALIDAPIKEY);
        }

        return new ResponseEntity<>(memberCardOnFileResponse, HttpStatus.OK);

    }

}
