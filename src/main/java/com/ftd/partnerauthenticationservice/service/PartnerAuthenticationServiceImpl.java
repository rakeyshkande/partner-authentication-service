package com.ftd.partnerauthenticationservice.service;

import com.ftd.partnerauthenticationservice.api.domain.request.MemberCardOnFileRequest;
import com.ftd.partnerauthenticationservice.api.domain.request.PartnerAuthenticationRequest;
import com.ftd.partnerauthenticationservice.api.domain.response.MemberCardOnFileResponse;
import com.ftd.partnerauthenticationservice.api.domain.response.PartnerAuthenticationResponse;
import com.ftd.partnerauthenticationservice.strategy.AuthorizeMember;
import com.ftd.partnerauthenticationservice.strategy.GetMemberCOFCredentials;
import com.ftd.partnerauthenticationservice.strategy.ValidateAPIKey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ftd.partnerauthenticationservice.api.domain.SiteId;

@Service
public class PartnerAuthenticationServiceImpl implements PartnerAuthenticationServiceInterface {

    @Autowired
    private AuthorizeMember authorizeMember;

    @Autowired
    private GetMemberCOFCredentials getMemberCOFCredentials;

    @Autowired
    private ValidateAPIKey validateAPIKey;

    @Override
    public PartnerAuthenticationResponse authenticateMember(PartnerAuthenticationRequest request, SiteId siteId) {

        return authorizeMember.authenticateMember(request, siteId);
    }

    @Override
    public MemberCardOnFileResponse retrieveMemberCOFCredentials(MemberCardOnFileRequest request, SiteId siteId) {

        return getMemberCOFCredentials.retrieveMemberCOFCredentials(request, siteId);
    }

    @Override
    public Boolean validAPIKey(PartnerAuthenticationRequest request, SiteId siteId) {

        return validateAPIKey.validAPIKey(
                request.getApplicationName(),
                request.getApiKey(),
                request.getMemberCode(),
                request.getUserName(),
                siteId);
    }

   // @Override
    public Boolean validAPIKey(MemberCardOnFileRequest request, SiteId siteId) {

        return validateAPIKey.validAPIKey(
                request.getApplicationName(),
                request.getApiKey(),
                request.getMemberCode(),
                "NA",
                siteId);
    }
}
