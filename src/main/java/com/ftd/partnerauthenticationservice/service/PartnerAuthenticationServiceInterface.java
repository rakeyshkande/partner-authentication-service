package com.ftd.partnerauthenticationservice.service;

import com.ftd.partnerauthenticationservice.api.domain.SiteId;
import com.ftd.partnerauthenticationservice.api.domain.request.MemberCardOnFileRequest;
import com.ftd.partnerauthenticationservice.api.domain.request.PartnerAuthenticationRequest;
import com.ftd.partnerauthenticationservice.api.domain.response.MemberCardOnFileResponse;
import com.ftd.partnerauthenticationservice.api.domain.response.PartnerAuthenticationResponse;

public interface PartnerAuthenticationServiceInterface {

    PartnerAuthenticationResponse authenticateMember(PartnerAuthenticationRequest request,
                                                     SiteId siteId);

    MemberCardOnFileResponse retrieveMemberCOFCredentials(MemberCardOnFileRequest request,
                                                          SiteId siteId);

    Boolean validAPIKey(PartnerAuthenticationRequest request, SiteId siteId);
}
