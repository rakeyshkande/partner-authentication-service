package com.ftd.partnerauthenticationservice.strategy;

//import java.util.ArrayList;
//import java.math.BigInteger;
//import java.nio.charset.StandardCharsets;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.Arrays;
import java.util.List;

import org.bouncycastle.util.encoders.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.ftd.partnerauthenticationservice.api.domain.SiteId;
//import com.ftd.partnerauthenticationservice.api.domain.entity.PartnerApplications;
import com.ftd.partnerauthenticationservice.api.domain.entity.PartnerAuth;
import com.ftd.partnerauthenticationservice.api.domain.entity.PartnerMember;
import com.ftd.partnerauthenticationservice.api.domain.repository.PartnerAuthRepository;
import com.ftd.partnerauthenticationservice.api.domain.request.PartnerAuthenticationRequest;
import com.ftd.partnerauthenticationservice.api.domain.response.PartnerAuthenticationResponse;
//import org.bouncycastle.util.BigIntegers;
//import org.bouncycastle.util.encoders.Hex;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

@Component
public class AuthorizeMember {

    private static final Logger LOGGER = LoggerFactory.getLogger(AuthorizeMember.class);

    @Autowired
    private PartnerAuthRepository partnerAuthRepository;

    //@Value("${validation.validateCredentials}")
    //private boolean validateCredentials;

    @HystrixCommand(groupKey = "hystrixGroup", commandKey = "authenticateMemberCommand",
            threadPoolKey = "HystrixThreadPoolKey", fallbackMethod = "authenticateMemberFallback",
            ignoreExceptions = { Exception.class })
    public PartnerAuthenticationResponse authenticateMember(PartnerAuthenticationRequest request, SiteId siteId) {

        PartnerAuthenticationResponse partnerAuthenticationResponse = new PartnerAuthenticationResponse();

        // For Testing Purposes, return true for all request, unless and exception were to occur.
        boolean authorizedUser = false; //true;

        LOGGER.info("AuthorizeMember calling authenticateMember with request: Member {}, Username {}",
                request.getMemberCode(), request.getUserName());

        request.setMemberCode(request.getMemberCode().toUpperCase());

        String partnerName = siteId.toString();

        try {
/*
            List<PartnerAuth> partnerAuthList = null;

            if (validateCredentials) {
                //List<PartnerAuth> partnerAuthList = partnerAuthRepository.authenticateMemberUserPassword(
                partnerAuthList = partnerAuthRepository.authenticateMemberUserPassword(
                        partnerName,
                        request.getApplicationName(),
                        true,
                        request.getMemberCode(),
                        request.getUserName(),
                        //request.getPassword(),
                        true);
            } else {

                partnerAuthList = partnerAuthRepository.authenticateMember(
                        partnerName,
                        request.getApplicationName(),
                        true,
                        request.getMemberCode(),
                        true);
            }
*/
            List<PartnerAuth> partnerAuthList = partnerAuthRepository.authenticateMemberUserPassword(
                    partnerName,
                    request.getApplicationName(),
                    true,
                    request.getMemberCode(),
                    request.getUserName(),
                    //request.getPassword(),
                    true);

            // If Null Response - return authorized as false
            // Possibly narrow down error
            if (partnerAuthList == null || partnerAuthList.isEmpty()) {

                LOGGER.info("partnerAuthRepository.authenticateMember - authorizedUser - No match found. ",
                        partnerAuthList);
                authorizedUser = false;

            } else {

                // If Not Null
                //      Parse response for Member data
                //      Set Response values for the Members Username, password and groupid
                //LOGGER.info("partnerAuthRepository.authenticateMember - authorizedUser = true.");
/*
                if (validateCredentials) {
                    LOGGER.info("partnerAuthRepository.authenticateMemberUserPassword " +
                            "- Match found. ", partnerAuthList);
                } else {
                    LOGGER.info("partnerAuthRepository.authenticateMember - Match found. ", partnerAuthList);
                }
*/
                LOGGER.info("partnerAuthRepository.authenticateMemberUserPassword " +
                        "- Match found. ", partnerAuthList);

                for (PartnerAuth paAuth : partnerAuthList) {

                    if (paAuth.getPartnerName().equals(partnerName) &&
                            paAuth.getApplicationName().equals(request.getApplicationName())) {

                        List<PartnerMember> partnerMemberList = paAuth.getMembers();

                        for (PartnerMember paMbr : partnerMemberList) {

                            if (paMbr.getCode().equals(request.getMemberCode())) {

                                /*
                                if (!validateCredentials) {
                                    authorizedUser = true;
                                    break;
                                }
                                */

                                String memberPassword = request.getPassword();
                                //byte[] memberPasswordByte = memberPassword.getBytes(StandardCharsets.UTF_8);

                                MessageDigest digest = MessageDigest.getInstance("SHA-256");
                                digest.reset();
                                byte[] memberPasswordByte = memberPassword.getBytes(StandardCharsets.UTF_8);
                                digest.update(memberPasswordByte);
                                byte[] memberPasswordByteTEST = digest.digest();
                                //String hexStr = "";
                                //for (int i = 0; i < memberPasswordByteTEST.length; i++) {
                                //    hexStr +=  Integer.toString( ( memberPasswordByteTEST[i] & 0xff ) +
                                // 0x100, 16).substring( 1 );
                                //}
                                byte[] encoded = Hex.encode(memberPasswordByteTEST);

                                //memberPasswordByte = digest.digest(memberPasswordByte);
                                // byte[] memberPasswordByte =
                                //        digest.digest(memberPassword.getBytes(StandardCharsets.UTF_8));
                                //String strMemberPasswordByte = memberPasswordByte.toString();

                                //BigInteger no = new BigInteger(1,memberPasswordByte);
                                //String hashtext = no.toString(16);

                                //String sha256hex = new String(Hex.encode(memberPasswordByte));


                                String storedPassword = paMbr.getPassword().toLowerCase();
                                storedPassword = storedPassword.replace("0x", "");

                                byte[] storedPasswordHash = storedPassword.getBytes(StandardCharsets.UTF_8);
                                //String xxx = new String(storedPasswordHash,"UTF-8");

                                if (Arrays.equals(storedPasswordHash, encoded)) {

                                    authorizedUser = true;
                                }

                                break;
                            }
                        }

                    }
                }
            }
        } catch (Exception e) {
            LOGGER.error("authenticateMember Exception occurred! {}", e.getLocalizedMessage());
            authorizedUser = false;
        }

        partnerAuthenticationResponse.setAuthorized(authorizedUser);

        LOGGER.info("authenticateMember response {} ", partnerAuthenticationResponse);

        return partnerAuthenticationResponse;

    }

    public PartnerAuthenticationResponse authenticateMemberFallback(PartnerAuthenticationRequest request,
                                                                    SiteId siteId) {


        LOGGER.error("Sending a falllback for authenticateMember with request: Member {}, Username {}",
                request.getMemberCode(), request.getUserName());


        PartnerAuthenticationResponse fallbackResponse = new PartnerAuthenticationResponse();
        fallbackResponse.setAuthorized(false);
        return fallbackResponse;
    }

}
