package com.ftd.partnerauthenticationservice.strategy;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ftd.partnerauthenticationservice.api.domain.SiteId;
import com.ftd.partnerauthenticationservice.api.domain.entity.PartnerAuth;
import com.ftd.partnerauthenticationservice.api.domain.repository.PartnerAuthRepository;
//import com.ftd.partnerauthenticationservice.api.domain.request.MemberCardOnFileRequest;
//import com.ftd.partnerauthenticationservice.api.domain.request.PartnerAuthenticationRequest;
import com.ftd.partnerauthenticationservice.util.EncryptDecrypt;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

@Component
public class ValidateAPIKey {

    private static final Logger LOGGER = LoggerFactory.getLogger(AuthorizeMember.class);

    @Autowired
    private PartnerAuthRepository partnerAuthRepository;

    @Autowired
    private EncryptDecrypt encryptDecrypt;

    /*
        public Boolean validAPIKey(PartnerAuthenticationRequest request, SiteId siteId) {

            Boolean valid = false;

            String apiKey;

            LOGGER.info("authenticateMember calling validAPIKey with member:{} username: {}",
                    request.getMemberCode(), request.getUserName());

            String partnerName = siteId.toString();

            try {
                List<PartnerAuth> partnerAuthList = partnerAuthRepository.getAPIKey(
                        partnerName,
                        request.getApplicationName(),
                        true);

                if (partnerAuthList == null || partnerAuthList.isEmpty()) {

                    LOGGER.info("partnerAuthRepository.getAPIKey - No match found. valid = false");

                    valid = false;

                } else {

                    LOGGER.info("partnerAuthRepository.getAPIKey  - Match found. ");

                    for (PartnerAuth paAuth : partnerAuthList) {

                        if (paAuth.getPartnerName().equals(partnerName)) {

                            apiKey = paAuth.getApiKey();

                            //String testApiKey = "2ManySecrets";
                            // "475e4f18-faa7-4c42-8204-56a842c5dfe6";
                            //"mRp1U1NdaRt7EGvWD+FtJ8YdbAY9Ekojx5YyeDbCdpB6ltmbEVfMb/mr05cx8wy9";

                            //String testEncryptAPIKey = encryptDecrypt.encrypt(testApiKey);

                            // 1) Decrypt apiKey
                            apiKey = encryptDecrypt.decrypt(apiKey);

                            // 2) Compare Request.apiKey to decrypted API Key
                            if (apiKey.equals(request.getApiKey())) {
                                //  Return TRUE if Key matches
                                valid = true;
                                LOGGER.info("partnerAuthRepository.getAPIKey  - Match found. Valid = true");
                            } else {

                                //  Return FALSE if Key does not match
                                valid = false;
                                LOGGER.info("partnerAuthRepository.getAPIKey  - Match found. Valid = false");
                            }

                            break;
                        }
                    }

                }
            } catch (Exception e) {
                LOGGER.error("authenticateMember Exception occurred! {}", e.getLocalizedMessage());
                valid = false;
            }

            return valid;

        }

    */
    @HystrixCommand(groupKey = "hystrixGroup", commandKey = "validAPIKeyCommand",
            threadPoolKey = "HystrixThreadPoolKey", fallbackMethod = "validApiKeyFallback",
            ignoreExceptions = { Exception.class })
    public Boolean validAPIKey(String applicationName, String partnerApiKey, String memberCode,
                               String userName, SiteId siteId) {

        Boolean valid = false;

        String apiKey;

        LOGGER.info("authenticateMember calling validAPIKey with applicationName:{} member:{} username: {}",
                applicationName, memberCode, userName);

        String partnerName = siteId.toString();

        try {
            List<PartnerAuth> partnerAuthList = partnerAuthRepository.getAPIKey(
                    partnerName,
                    applicationName,
                    true);

            if (partnerAuthList == null || partnerAuthList.isEmpty()) {

                LOGGER.info("applicationName: {} - partnerAuthRepository.getAPIKey - No match found. valid = false",
                        applicationName);

                valid = false;

            } else {

                LOGGER.info("applicationName: {} - partnerAuthRepository.getAPIKey  - Match found. ",
                        applicationName);

                for (PartnerAuth paAuth : partnerAuthList) {

                    if (paAuth.getPartnerName().equals(partnerName)) {

                        apiKey = paAuth.getApiKey();

                        //String testApiKey = "2ManySecrets";
                        // "475e4f18-faa7-4c42-8204-56a842c5dfe6";
                        //"mRp1U1NdaRt7EGvWD+FtJ8YdbAY9Ekojx5YyeDbCdpB6ltmbEVfMb/mr05cx8wy9";

                        //String testEncryptAPIKey = encryptDecrypt.encrypt(testApiKey);

                        // 1) Decrypt apiKey
                        apiKey = encryptDecrypt.decrypt(apiKey);

                        // 2) Compare Request.apiKey to decrypted API Key
                        if (apiKey.equals(partnerApiKey)) {
                            //  Return TRUE if Key matches
                            valid = true;
                            LOGGER.info("applicationName: {} - partnerAuthRepository.getAPIKey" +
                                    "  - Match found. Valid = true", applicationName);
                        } else {

                            //  Return FALSE if Key does not match
                            valid = false;
                            LOGGER.info("applicationName: {} - partnerAuthRepository.getAPIKey" +
                                    "  - Match found. Valid = false", applicationName);
                        }
                        break;
                    }
                }
            }
        } catch (Exception e) {
            LOGGER.error("validApiKey - applicationName: {}" +
                    " - authenticateMember Exception occurred! {}", applicationName, e.getLocalizedMessage());
            valid = false;
        }
        return valid;
    }

    public Boolean validApiKeyFallback(String applicationName, String partnerApiKey, String memberCode,
                                       String userName, SiteId siteId) {

        LOGGER.error("Sending a fallback for validAPIKey with applicationName:{} member:{} username: {}",
                applicationName, memberCode, userName);

        return false;
    }
}
