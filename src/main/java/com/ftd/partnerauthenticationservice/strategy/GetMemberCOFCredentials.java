package com.ftd.partnerauthenticationservice.strategy;


//import java.util.Random;

//import static java.lang.Math.abs;

//import java.nio.charset.StandardCharsets;
//import java.security.MessageDigest;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ftd.partnerauthenticationservice.api.domain.SiteId;
//import com.ftd.partnerauthenticationservice.api.domain.entity.PartnerApplications;
import com.ftd.partnerauthenticationservice.api.domain.entity.PartnerAuth;
import com.ftd.partnerauthenticationservice.api.domain.entity.PartnerMember;
import com.ftd.partnerauthenticationservice.api.domain.repository.PartnerAuthRepository;
import com.ftd.partnerauthenticationservice.api.domain.request.MemberCardOnFileRequest;
import com.ftd.partnerauthenticationservice.api.domain.response.MemberCardOnFileResponse;
import com.ftd.partnerauthenticationservice.util.EncryptDecrypt;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

@Component
public class GetMemberCOFCredentials {

    private static final Logger LOGGER = LoggerFactory.getLogger(AuthorizeMember.class);

    @Autowired
    private PartnerAuthRepository partnerAuthRepository;

    @Autowired
    private EncryptDecrypt encryptDecrypt;

    @HystrixCommand(groupKey = "hystrixGroup", commandKey = "cofCredentialsCommand",
            threadPoolKey = "HystrixThreadPoolKey", fallbackMethod = "retrieveMemberCOFCredentialsFallback",
            ignoreExceptions = { Exception.class })
    public MemberCardOnFileResponse retrieveMemberCOFCredentials(MemberCardOnFileRequest request,
                                                                 SiteId siteId) {

        boolean authorizedUser = false;

        LOGGER.info("retrieveMemberCOFCredentials calling GetMemberCOFCredentials with request: {}", request);

        MemberCardOnFileResponse memberCardOnFileResponse = new MemberCardOnFileResponse();

        request.setMemberCode(request.getMemberCode().toUpperCase());

        memberCardOnFileResponse.setMemberCode(request.getMemberCode());

        String partnerName = siteId.toString();

        // Query Mongo Database
        try {
            List<PartnerAuth> partnerAuthList = partnerAuthRepository.getMemberCOFCredentials(
                    partnerName,
                    //request.getApiKey(),
                    request.getApplicationName(),
                    true,
                    request.getMemberCode(),
                    true);

            // If Null Response - return authorized as false
            // Possibly narrow down error
            if (partnerAuthList == null || partnerAuthList.isEmpty()) {

                LOGGER.info("partnerAuthRepository.getMemberCOFCredentials - No match found. ", partnerAuthList);

                authorizedUser = false;
            } else {

                // If Not Null
                //      Parse response for Member data
                //      Set Response values for the Members Username, password and groupid

                LOGGER.info("partnerAuthRepository.getMemberCOFCredentials - Match found. ", partnerAuthList);

                for (PartnerAuth paAuth : partnerAuthList) {

                    if (paAuth.getPartnerName().equals(partnerName) &&
                            paAuth.getApplicationName().equals(request.getApplicationName())) {

//                        List<PartnerApplications> partnerApplicationsList = paAuth.getApplications();

//                        for (PartnerApplications paApps : partnerApplicationsList) {

//                            if (paAuth.getApplicationName().equals(request.getApplicationName())) {

                        List<PartnerMember> partnerMemberList = paAuth.getMembers();

                        for (PartnerMember paMbr : partnerMemberList) {

                            if (paMbr.getCode().equals(request.getMemberCode())) {

                                authorizedUser = true;

                                memberCardOnFileResponse.setGroupId(paMbr.getGroupId());

                                memberCardOnFileResponse.setUserName(paMbr.getUserName());

                                // TODO52 CarlDecrypt Password and populate response
                                String encPassword = paMbr.getPassword();
                                String unEncPassword = encryptDecrypt.decrypt(encPassword);

                                memberCardOnFileResponse.setPassword(unEncPassword);

                                //MessageDigest digest = MessageDigest.getInstance("SHA-256");
                                //String text = memberCardOnFileResponse.getPassword();
                                //byte[] hash = digest.digest(text.getBytes(StandardCharsets.UTF_8));

                                break;
                            }
                        }
                        //                           }
                        //                       }
                    }
                }
            }
        } catch (Exception e) {
            LOGGER.error("retrieveMemberCOFCredentials Exception occurred! {}", e.getLocalizedMessage());
            authorizedUser = false;
        }

        // Adding this to avoid any issues on first drop

        // authorizedUser = true;

        // Random rand = new Random();
        // //int randBound = rand.nextInt();
        // memberCardOnFileResponse.setGroupId(abs(rand.nextInt()));

        // memberCardOnFileResponse.setUserName("TestUserName");

        // memberCardOnFileResponse.setPassword("TestPassword");

        // End of test code for first drop

        memberCardOnFileResponse.setAuthorized(authorizedUser);

        return memberCardOnFileResponse;
    }

    public MemberCardOnFileResponse retrieveMemberCOFCredentialsFallback(MemberCardOnFileRequest request,
                                                                         SiteId siteId) {


        LOGGER.error("Sending fallback for GetMemberCOFCredentials with request: {}", request);

        MemberCardOnFileResponse fallbackCardOnFileResponse = new MemberCardOnFileResponse();
        fallbackCardOnFileResponse.setAuthorized(false);
        fallbackCardOnFileResponse.setMemberCode(request.getMemberCode());

        return fallbackCardOnFileResponse;
    }
}
